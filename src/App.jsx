import { useState, useEffect, useContext } from "react";
import "./App.css";
import { Routes, Route } from "react-router-dom";
import Header from "./components/header/Header";
import Home from "./components/home/Home";
import BoardDetails from "./components/boardDetails/BoardDetails";
import { useNavigate } from "react-router-dom";
import { fetchData, postData } from './api';
import { deleteData } from './api';
import Loader from "./loader/Loader";
import { AlertContext } from "./components/alert/AlertContext";
import ErrorAlert from "./components/error/Error";


function App() {
  const [boards, setBoards] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const { handleOpen } = useContext(AlertContext);

  const navigate = useNavigate();


  useEffect(() => {
    fetchData("members/me/boards").then(({ data, result }) => {
      if (result === true) {
        setBoards(data);
      } else {
        handleOpen(data, "error");
      }
    });
    setIsLoading(false);
  }, []);


  const addBoard = (inputValue) => {
    postData(`boards?name=${inputValue}`).then(({ data, result }) => {
      if (result === true) {
        setBoards((value) => {
          return [...value, data];
        });
      } else {
        handleOpen(data,"error");
      }
    });
  };

  const deleteBoardHandler = (id) => {
    deleteData(`boards/${id}`).then(({ data, result }) => {
      if (result === true) {
        const updatedBoards = boards.filter((board) => board.id !== id);
        setBoards(updatedBoards);
        navigate('/')
      } else {
        handleOpen(data,"error");
      }
    });
  };

  return (
    <>
      <Header />
      <ErrorAlert/>
      <Routes>
        <Route
          path="/"
          element={
            isLoading ? (
              <Loader />
            ) : (
              <Home boards={boards} addBoard={addBoard} />
            )
          }
        />
        <Route
          path="/boards/:id"
          element={
            <BoardDetails
              boards={boards}
              onDeleteBoardHandler={deleteBoardHandler}
            />
          }
        />
      </Routes>
    </>
  );
}

export default App;
