import axios from 'axios';
const baseUrl = "https://api.trello.com/1";
const token =
    "ATTA6d0eb5b33da341451bf8edc6f1cbb1de15163b5470b43b08028fb28e2bd2a276B25C5441";
const key = "412adf7733655b4b5da5636f5d2e8798";

export async function fetchData(endpoint) {
    try {
      const response = await axios.get(
        `${baseUrl}/${endpoint}?key=${key}&token=${token}`
      );
      return { data: response.data, result: true };
    } catch (error) {
      return { data: error.message, result: false };
    }
  }
  

export async function postData(endpoint) {
    try {
        const response = await axios.post(
            `${baseUrl}/${endpoint}&key=${key}&token=${token}`
        );

        return { data: response.data, result: true };
    } catch (error) {
        return { data: error.message, result: false };
    }
}

export async function deleteData(endpoint) {
    try {
        const response = await axios.delete(
            `${baseUrl}/${endpoint}?key=${key}&token=${token}`
        );

        return { data: response.data, result: true };
    } catch (error) {
        return { data: error.message, result: false };
    }
}

export async function putData(endpoint) {
    try {
        const response = await axios.put(
            `${baseUrl}/${endpoint}&key=${key}&token=${token}`
        );

        return { data: response.data, result: true };
    } catch (error) {
        return { data: error.message, result: false };
    }
}
