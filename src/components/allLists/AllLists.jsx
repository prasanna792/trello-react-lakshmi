import {
    CardContent,
    Typography,
    Stack,
    Button,
    Card,
    Tooltip,
} from "@mui/material";
import React, { useState, useEffect,useContext } from "react";
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
import Cards from "../cards/Cards";
import CreateCard from "../createCard/CreateCard";
import { fetchData,postData,deleteData } from "../../api";
import ErrorAlert from "../error/Error";
import { AlertContext } from "../alert/AlertContext";


const AllLists = ({ item, deleteListHandler }) => {
    const [cards, setCards] = useState([]);
    const { handleOpen } = useContext(AlertContext);

    useEffect(() => {
        fetchData(`lists/${item.id}/cards`).then(({ data, result }) => {
            if (result === true) {
                setCards(data);
            } else {
                handleOpen(data,"error");
            }
        });
    }, []);

    const addCardHandler = (inputValue, id) => {
        postData(`lists/${id}/cards?name=${inputValue}`).then(({ data, result }) => {
            if (result === true) {
                setCards((prevValue) => [...prevValue, data]);
                console.log(cards, "cards");
            } else {
                handleOpen(data,"error");
            }
        });
    };

    function onDeleteListHandler() {
        deleteListHandler(item);
    }

    const deleteCard = (card) => {
        deleteData(`cards/${card.id}`).then(({ data, result }) => {
            if (result === true) {
                const updatedCards = cards.filter((item) => {
                    return card.id !== item.id;
                });
                setCards(updatedCards);
            } else {
                handleOpen(data,"error")
            }
        });
    };

    return (
        <div>
            <ErrorAlert/>
            <Card sx={{ minWidth: "300px" }} style={{ backgroundColor: '#F1F2F4', borderRadius: '13px' }}>
                <CardContent>
                    <Stack direction="row" spacing="auto" sx={{ minWidth: "100%" }} style={{ padding: '0 0.8rem', paddingRight: '0' }} >
                        <Typography variant="p" component="div" style={{ fontWeight: 'bold' }}>
                            {item.name}
                        </Typography>
                        <Tooltip title="Archive List">
                            <Button onClick={onDeleteListHandler}>
                                <DeleteOutlineIcon style={{ color: 'red', height: '15px' }} />
                            </Button>
                        </Tooltip>
                    </Stack>
                    <Stack direction="column" spacing={1.3} style={{ paddingTop: '1rem' }}>
                        {cards.length
                            ? cards.map((card) => {
                                return (
                                    <Cards
                                        key={card.id}
                                        card={card}
                                        deleteCard={deleteCard}
                                    />
                                );
                            })
                            : null}
                        <CreateCard id={item.id} addCardHandler={addCardHandler} />
                    </Stack>
                </CardContent>
            </Card>
        </div>
    );
};

export default AllLists;
