import React, { useState } from "react";
import { Card, Button, Stack, TextField, ButtonGroup } from "@mui/material";



const CreateCheckList = ({ id, onAddCheckListHandler }) => {
    const [showAddCheckListField, setShowAddCheckListField] = useState(false);
    const [inputValue, setInputValue] = useState("");

    const submitHandler = (e) => {
        e.preventDefault();
        onAddCheckListHandler(inputValue, id);
        setShowAddCheckListField(false);
    };

    return (
        <Card sx={{ width: "250px", marginTop: "10px" }}>
            {!showAddCheckListField && (
                <Button
                    size="medium"
                    style={{width:'100%'}}
                    onClick={() => {
                        setShowAddCheckListField(true);
                    }}
                >
                    <span style={{ color: "black",fontWeight:'bold' }}>+ Create Check List</span>
                </Button>
            )}
            {showAddCheckListField && (
                <form onSubmit={submitHandler} style={{ width: "100%",padding:'1rem' }}>
                    <Stack direction="column" spacing={2} sx={{ width: "100%" }}>
                        <TextField
                            multiline={true}
                            sx={{ width: "100%" }}
                            required
                            id="outlined-required"
                            label="Title"
                            variant="outlined"
                            size="small"
                            onChange={(e) => {
                                setInputValue(e.target.value);
                            }}
                        />
                        <ButtonGroup aria-label=" secondary button group">
                            <Button
                                type="submit"
                                size="small"
                                variant="contained"
                                sx={{ marginRight: "10px",fontWeight:'bold' }}
                            >
                                Add Checklist
                            </Button>
                            <Button
                                size="small"
                                variant="text"
                                onClick={() => {
                                    setShowAddCheckListField(false);
                                }}
                            >
                                X
                            </Button>
                        </ButtonGroup>
                    </Stack>
                </form>
            )}
        </Card>
    );
};

export default CreateCheckList;
