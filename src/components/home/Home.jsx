import React, { useState } from "react";
import { Button, Container, Grid, Typography } from "@mui/material";
import CreateBoard from "../createBoard/CreateBoard";
import Board from "../board/Board";
import './Home.css';
import { NavLink } from "react-router-dom";


const Home = ({ boards, addBoard }) => {
    const [showCreateBoard, setShowCreateBoard] = useState(false);

    return (
        <>
            <Container sx={{ width: "80%", margin: "auto", padding: "20px"}}>
                <Button
                    color="inherit"
                    onClick={() => setShowCreateBoard(true)}
                    variant="outlined"
                >
                    Create Board
                </Button>

                {showCreateBoard && (
                    <CreateBoard
                        addBoard={addBoard}
                        onCreateBoard={() => setShowCreateBoard(!showCreateBoard)}
                    />
                )}

                <Typography variant="h5" sx={{ flexGrow: 1, margin: "20px 0px" }}>
                    All Boards
                </Typography>
                <Grid container spacing={2} className="grid-boards">
                    {boards.length ? (
                        boards.map((item) => {
                            return (
                                <Grid item xs={12} sm={6} md={2} key={item.id} className="grid-item-boards" >
                                    <NavLink to={`/boards/${item.id}`} className="nav-link-no-underline">
                                    <Board key={item.id} item={item} />
                                    {/* <p>{item.name}</p> */}
                                    </NavLink>
                                </Grid>
                            );
                        })
                    ) : (
                        <Typography variant="h6" sx={{ flexGrow: 1, margin: "20px 0px" }}>
                            No Boards are Created...
                        </Typography>
                    )}
                </Grid>
            </Container>
        </>
    );
};

export default Home;


