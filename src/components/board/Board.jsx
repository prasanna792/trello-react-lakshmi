import { Card, Typography } from "@mui/material";
import React from "react";

const Board = ({ item }) => {

  return (
    <Card
      sx={{
        height: "150px",
        backgroundColor: `${item.prefs.backgroundColor}`,
        textAlign: "center",
        backgroundImage: `url(${item.prefs.backgroundImage})`,
        backgroundSize:'cover',
        fontWeight:'bold',
        textDecoration:'none'
      }}
    >
      <Typography variant="h6" color={"white"} sx={{marginTop:'20%'}}>{item.name}</Typography>
    </Card>
  );
};

export default Board;
