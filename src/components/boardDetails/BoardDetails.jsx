import { useEffect, useState,useContext } from "react";
import axios from 'axios';
import { Container, Stack, Button, Typography, Tooltip } from "@mui/material";
import CreateList from "../createList/CreateList";
import { useParams } from "react-router-dom";
import AllLists from "../allLists/AllLists";
import './BoardDetails.css';
import backgroundImage from '../../assets/background.png';
import DeleteIcon from '@mui/icons-material/Delete';
import { fetchData,postData,putData } from "../../api";
import Loader from "../../loader/Loader";
import { AlertContext } from "../alert/AlertContext";
import ErrorAlert from "../error/Error";


const BoardDetails = ({ boards, onDeleteBoardHandler }) => {
  const [lists, setLists] = useState([]);
  const [isLoading,setIsLoading] = useState(true);
  const { handleOpen } = useContext(AlertContext);

  const { id } = useParams();

  useEffect(() => {
    fetchData(`boards/${id}/lists`).then(({ data, result }) => {
      if (result === true) {
        setLists(data);
      } else {
        handleOpen(data,"error")
      } 
    });
    setIsLoading(false);
  }, [id]);

  const selectedBoard = boards.find((item) => {
    console.log(item.id, "item-id")
    return id === item.id;
  });

  const values = boards.filter((item) => {
    // console.log(item.id, "item-id")
    return id === item.id;
  });

  console.log(values,"values")

  const deleteBoardHandler = () => {
    onDeleteBoardHandler(id);
  };

  const addListHandler = (inputVal, id) => {
    postData(`boards/${id}/lists?name=${inputVal}`).then(({ data, result }) => {
      console.log(data, "list-data");
      if (result === true) {
        setLists((prevValue) => {
          return [...prevValue, data];
        });
      } else {
        handleOpen(data,"error");
      }
    });
  };


  const deleteListHandler = (item) => {
    putData(`lists/${item.id}?closed=true`).then(({ data, result }) => {
      if (result === true) {
        const updatedList = lists.filter((ele) => ele.id !== item.id);
        setLists(updatedList);
      } else {
        handleOpen(data,"error");
      }
    });
  };

  if(isLoading){
    return <Loader/>
  }

  return (
    <>
    <ErrorAlert/>
      <Container style={{
        backgroundImage: `url(${backgroundImage})`,
        backgroundRepeat: 'repeat',
        backgroundSize: 'cover',
        height: '100vh',
        overflowY: 'scroll',
      }}
        sx={{ overflowX: "auto", minWidth: "100%", minHeight: "100vh" }}
      >
        <Stack direction={"row"} justifyContent={'space-between'} style={{backgroundColor:'#F1F2F4',width:'20%',marginTop:'0.8rem',color:'black',borderRadius:'5px',marginLeft:'0.8rem'}}>
          <Typography variant="h5" color={"black"} sx={{ padding: "10px" }}>
            {selectedBoard ? selectedBoard.name : ""}
          </Typography>
          <Tooltip title="Delete Board">
            <Button onClick={deleteBoardHandler}>
              <DeleteIcon style={{ color: 'red' }} />
            </Button>
          </Tooltip>
        </Stack>

        <Stack
          spacing={2}
          direction="row"
          sx={{ margin: "auto", padding: "10px" }}
        >
          {selectedBoard && lists.length
            ? lists.map((item) => {
              return (
                <AllLists
                  key={item.id}
                  item={item}
                  deleteListHandler={deleteListHandler}
                />
              );
            })
            : null}
          <CreateList id={id} addListHandler={addListHandler} />
        </Stack>
      </Container>

    </>
  );
};

export default BoardDetails;
