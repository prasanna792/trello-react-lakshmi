import {
  Dialog,
  DialogContent,
  Button,
  Stack,
  Typography,
  Tooltip
} from "@mui/material";
import React, { useEffect, useState,useContext } from "react";
import CreateCheckList from "../createCheckList/CreateCheckList";
import CheckList from "../checkList/CheckList";
import { fetchData,postData,deleteData } from "../../api";
import ErrorAlert from "../error/Error";
import { AlertContext } from "../alert/AlertContext";


const CardDetails = ({ card, show, onClose }) => {
  const [checkLists, setCheckLists] = useState([]);
  const { handleOpen } = useContext(AlertContext);

  useEffect(() => {
    fetchData(`cards/${card.id}/checklists`).then(({ data, result }) => {
      if (result === true) {
        setCheckLists(data);
      } else {
        handleOpen(data,"error")
      }
    });

  }, []);

  const addCheckListHandler = (inputValue, id) => {
    postData(`cards/${id}/checklists?name=${inputValue}`).then(
      ({ data, result }) => {
        if (result === true) {
          setCheckLists((prevCheckList) => {
            return [...prevCheckList, data];
          });
        } else {
          handleOpen(data,"error");
        }
      }
    );
  };

  const deleteCheckListHandler = (checklistId) => {
    deleteData(`checklists/${checklistId}`).then(({ data, result }) => {
      if (result === true) {
        const updatedChecklists = checkLists.filter((item) => {
          return item.id !== checklistId;
        });
        setCheckLists(updatedChecklists);
      } else {
        handleOpen(data,"error")
      }
    });
  };


  return (
    <>
    <ErrorAlert/>
      <Dialog open={show} onClose={onClose} >
        <div
          style={{
            padding: "10px",
            minHeight: "600px",
            justifyContent: 'space-between',
            minWidth: '600px',
            backgroundColor: '#F1F2F4',
            overflowX: 'hidden'
          }}
        >
          <DialogContent>
            <Stack direction={"row"} spacing={10} justifyContent={'space-between'} marginBottom={'1rem'}>

              <Typography variant="h6" sx={{ fontWeight: 'bold' }}>
                {card.name}
              </Typography>
              <Tooltip title='close'>
                <Button onClick={onClose} style={{ display: 'flex', color: 'red', marginRight: '-1rem' }}>X</Button>
              </Tooltip>
            </Stack>
            <Stack direction={"column"} spacing={2}>
              {checkLists.length
                ? checkLists.map((item) => {
                  return (
                    <CheckList
                      checklist={item}
                      key={item.id}
                      ondeleteChecklist={deleteCheckListHandler}
                    />
                  );
                })
                : null}
            </Stack>
            <CreateCheckList
              id={card.id}
              onAddCheckListHandler={addCheckListHandler}
            />
          </DialogContent>
        </div>
      </Dialog>

    </>
  );
};

export default CardDetails;
