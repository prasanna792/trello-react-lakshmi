import React, { useState } from "react";
import { Button, Card, Stack, Tooltip, Typography } from "@mui/material";
import CardDetails from "../cardDetails/CardDetails";


const Cards = ({ card, deleteCard }) => {
    const [showCardDetails, setShowCardDetails] = useState(false);

    function deleteCardHandler(e) {
        e.stopPropagation();
        deleteCard(card)
    }

    const HandleModalOpen = () => {
        setShowCardDetails(true);
    };

    const HandleModalClose = () => {
        setShowCardDetails(false);
    };

    return (
        <>
            <Card
                sx={{ padding: "3px 10px", minWidth: "280px" }}
                style={{ backgroundColor: '#FFFFFF', textAlign: 'center', cursor: 'pointer' }}
                onClick={HandleModalOpen}
            >
                <Stack direction="row" spacing="auto" style={{ alignItems: 'center' }}   >
                    <Typography variant="body1" style={{ color: '#36454F' }} >{card.name}</Typography>

                    <Tooltip title="Delete card" onClick={deleteCardHandler}>
                        <Button style={{ color: 'red' }}>x</Button>
                    </Tooltip>
                </Stack>
            </Card>

            <CardDetails
                card={card}
                show={showCardDetails}
                onClose={HandleModalClose}
            />

        </>
    );
};

export default Cards;


