import React from "react";
import { ListItem, Checkbox, ListItemText, Button,Tooltip } from "@mui/material";

const CheckListItem = ({ item, cardId, onChangeState, onDeleteItem }) => {
    const handleCheckBox = (event) => {
        const checked = event.target.checked;
        const checkedState = checked ? "complete" : "incomplete";
        onChangeState(item, cardId, checkedState);
    };

    const deleteItemHandler = () => {
        onDeleteItem(item.idChecklist, item.id)
    }

    return (
        <ListItem>
            <Checkbox
                onChange={handleCheckBox}
                color="primary"
                inputProps={{ "aria-label": item.name }}
                checked={item.state === "complete" ? true : false}
            />
            <ListItemText primary={item.name} />
            <Tooltip title='Delete check item'>
            <Button onClick={deleteItemHandler} style={{color:'red'}}>X</Button>
            </Tooltip>
           
        </ListItem>
    );
};

export default CheckListItem;
