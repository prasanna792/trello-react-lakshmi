import React, { useState } from "react";
import { Card, Button, Stack, TextField, ButtonGroup } from "@mui/material";


const CreateCard = ({ id, addCardHandler }) => {
  const [showAddCardField, setShowAddCardField] = useState(false);
  const [inputValue, setInputValue] = useState("");

  const submitHandler = (event) => {
    event.preventDefault();

    addCardHandler(inputValue, id);
    setShowAddCardField(false);
  };

  return (
    <Card sx={{ width: "280px" }} >
      {!showAddCardField && (
        <Button
          size="medium"
          style={{fontWeight:'bold',width:'100%'}}
          onClick={() => {
            setShowAddCardField(true);
          }}
        >
          + Add Card
        </Button>
      )}
      {showAddCardField && (
        <form onSubmit={submitHandler} style={{ width: "100%",padding:'1rem' }} >
          <Stack direction="column" spacing={2} sx={{ width: "100%" }}>
            <TextField
              multiline={true}
              sx={{ width: "100%" }}
              required
              id="outlined-required"
              label="Card Title"
              variant="outlined"
              size="small"
              onChange={(e) => {
                setInputValue(e.target.value);
              }}
            />
            <ButtonGroup aria-label=" secondary button group">
              <Button
                type="submit"
                size="small"
                variant="contained"
                sx={{ marginRight: "10px" }}
                style={{fontWeight:'bold'}}
              >
                Add list
              </Button>
              <Button
                size="small"
                variant="text"
                onClick={() => {
                  setShowAddCardField(false);
                }}
              >
                X
              </Button>
            </ButtonGroup>
          </Stack>
        </form>
      )}
    </Card>
  );
};

export default CreateCard;
