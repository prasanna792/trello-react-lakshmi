import React, { useState } from "react";
import {
    Button,
    AppBar,
    Toolbar,
    IconButton,
    Typography,
} from "@mui/material";
import { NavLink } from "react-router-dom";


const Header = () => {
    return (
        <AppBar position="static" sx={{backgroundColor:'#27262B'}} >
            <Toolbar>
                <NavLink to={"/"}>
                    <Button style={{ color: "white", border: '2px solid white' }} variant="outlined" >Home</Button>
                </NavLink>
                <IconButton
                    color="inherit"
                    sx={{ marginLeft: "40%" }}
                >
                    <i className="fa-brands fa-trello"></i>
                </IconButton>
                <Typography variant="h6" sx={{ flexGrow: 1 }}>
                    Trello
                </Typography>

            </Toolbar>
        </AppBar>

    );
};

export default Header;
