import { useState, useEffect,useContext } from "react";
import { Card, Stack, Typography, Tooltip, Button, List } from "@mui/material";
import CheckListItem from "../checkListItem/CheckListItem";
import CreateItem from "../createCheckListItem/CreateCheckListItem";
import ProgressBar from "./ProgressBar";
import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline';
import { fetchData, postData, putData, deleteData } from "../../api";
import ErrorAlert from "../error/Error";
import { AlertContext } from "../alert/AlertContext";


const CheckList = ({ checklist, ondeleteChecklist }) => {
    const [items, setItems] = useState([]);
    const { handleOpen } = useContext(AlertContext);

    useEffect(() => {

        fetchData(`checklists/${checklist.id}/checkItems`)
        .then(({ data, result }) => {
                if (result === true) {
                    setItems(data);
                }else {
                    handleOpen(data,"error")
                }
            }
        );
    }, [checklist.id]);

    const completedItems = items.filter((item) => item.state === "complete");
    const progress = (completedItems.length / items.length) * 100;

    const changeStateHandler = (item, cardId, state) => {
        putData(`cards/${cardId}/checkItem/${item.id}?state=${state}`).then(
            ({ data, result }) => {
                if (result === true) {
                    const updatedItems = items.map((ele) => {
                        if (item.id === ele.id) {
                            return { ...ele, state: state };
                        } else {
                            return ele;
                        }
                    });
                    setItems(updatedItems);
                } else {
                    handleOpen(data,"error");
                }
            }
        );
    };

    const deleteCheckList = () => {
        ondeleteChecklist(checklist.id);
    };

    const addItemHandler = (inputVal, id) => {
        postData(`checklists/${id}/checkItems?name=${inputVal}`).then(
            ({ data, result }) => {
                if (result === true) {
                    setItems((oldItems) => [...oldItems, data]);
                } else {
                    handleOpen(data,"error")
                }
            }
        );
    };

    const deleteItemHandler = (checklistId, id) => {
        deleteData(`checklists/${checklistId}/checkItems/${id}`).then(
            ({ data, result }) => {
                if (result === true) {
                    const updatedItems = items.filter((ele) => {
                        return ele.id !== id;
                    });
                    setItems(updatedItems);
                } else {
                    handleOpen(data,"error");
                }
            }
        );
    };

    return (
        <>
        <ErrorAlert/>
            <Card
                sx={{
                    padding: "20px",
                    minWidth: "280px",
                    boxShadow: "2px 2px 4px rgba(0, 0, 0, 0.3)",
                }}
            >
                <Stack direction="row" spacing="auto" justifyContent={'space-between'} alignItems={'center'} marginBottom={'10px'} >
                    <div style={{ display: 'flex', gap: '0.5rem', alignItems: 'center' }}>
                        <CheckCircleOutlineIcon fontSize="16px" />
                        <Typography variant="span" sx={{ fontWeight: 'bold' }}>{checklist.name}</Typography>
                    </div>

                    <div style={{ marginRight: '-1.5rem' }}>
                        <Tooltip title="Delete checklist">
                            <Button onClick={deleteCheckList} style={{ color: 'red' }}>X</Button>
                        </Tooltip>
                    </div>
                </Stack>

                <span >{items.length ? Math.floor(progress) : 0}% </span>
                <span ><ProgressBar value={items.length ? progress : 0} /></span>

                <List>
                    {items.length
                        ? items.map((item) => {
                            return (
                                <CheckListItem
                                    item={item}
                                    key={item.id}
                                    cardId={checklist.idCard}
                                    onChangeState={changeStateHandler}
                                    onDeleteItem={deleteItemHandler}
                                />
                            );
                        })
                        : null}
                </List>
                <CreateItem id={checklist.id} onAddItemHandler={addItemHandler} />
            </Card>

        </>
    );
};

export default CheckList;
