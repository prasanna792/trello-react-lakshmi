import { Stack, TextField, Card, Button } from "@mui/material";
import React, { useState } from "react";
import "./CreateBoard.css";


const CreateBoard = ({ addBoard, onCreateBoard }) => {
    const [inputVal, setInputVal] = useState("");


    const submitHandler = (event) => {
        event.preventDefault();
        addBoard(inputVal);
        onCreateBoard();
    };

    return (
        <form onSubmit={submitHandler} className="form-container">
            <Stack spacing={4} sx={{ paddingTop: "10px" }} direction={"column"} >
                <TextField
                    required
                    id="outlined-required"
                    label="Board Title"
                    variant="outlined"
                    size='small'
                    helperText={inputVal ? "" : "Board title is required"}
                    onChange={(e) => {
                        setInputVal(e.target.value);
                    }}  
                    
                />
                <Button variant="contained" color="primary" type="submit">
                    Create
                </Button>{" "}
            </Stack>
        </form>
    );
};

export default CreateBoard;
