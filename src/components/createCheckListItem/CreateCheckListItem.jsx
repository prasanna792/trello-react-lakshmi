import React, { useState } from "react";
import { Card, Button, Stack, TextField, ButtonGroup } from "@mui/material";

const CreateItem = ({ id, onAddItemHandler }) => {
    const [showAddItemField, setShowAddItemField] = useState(false);
    const [inputValue, setInputValue] = useState("");

    
    const submitHandler = (event) => {
        event.preventDefault();
        setShowAddItemField(false);
        // console.log("Submitting item:", inputValue); 
        onAddItemHandler(inputValue, id);
    };

    return (
        <Card sx={{ width: "200px" }}>
            {!showAddItemField && (
                <Button
                    size="small"
                   style={{width:'100%',fontWeight:'bold',backgroundColor:'#176B87',color:'white'}}
                    onClick={() => {
                        setShowAddItemField(true);
                    }}
                >
                   + add check item
                </Button>
            )}
            {showAddItemField && (
                <form onSubmit={submitHandler} style={{ width: "100%" }}>
                    <Stack direction="column" spacing={2} sx={{ width: "100%",padding:'1rem' }}>
                        <TextField
                            multiline={true}
                            sx={{ width: "100%" }}
                            required
                            id="outlined-required"
                            label="Title"
                            variant="outlined"
                            size="small"
                            onChange={(e) => {
                                setInputValue(e.target.value);
                            }}
                        />
                        <ButtonGroup aria-label=" secondary button group">
                            <Button
                                type="submit"
                                size="small"
                                variant="contained"
                                sx={{ marginRight: "10px" }}
                            >
                                Add item
                            </Button>
                            <Button
                                size="small"
                                variant="text"
                                onClick={() => {
                                    setShowAddItemField(false);
                                }}
                            >
                                X
                            </Button>
                        </ButtonGroup>
                    </Stack>
                </form>
            )}
        </Card>
    );
};

export default CreateItem;
