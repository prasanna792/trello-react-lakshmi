import React, { useState } from "react";
import { Card, Button, Stack, TextField, ButtonGroup } from "@mui/material";


const CreateList = ({ id, addListHandler }) => {
  const [showAddListField, setShowAddListField] = useState(false);
  const [inputValue, setInputValue] = useState("");

  const submitHandler = (event) => {
    event.preventDefault();

    addListHandler(inputValue, id);
    setShowAddListField(false);
  };

  return (
    <div>
      <Card sx={{ minWidth: "300px" }}>
        {!showAddListField && (
          <Button
            size="large"
            onClick={() => {
              setShowAddListField(true);
            }}
            style={{width:'100%',fontWeight:'bold'}}
            sx={{ color: "black",}}
          >
            + Create a new list
          </Button>
        )}
        {showAddListField && (
          <form onSubmit={submitHandler} style={{ minWidth: "100%",padding:'1rem' }}>
            <Stack direction="column" spacing={2} sx={{ width: "100%" }}>
              <TextField
                sx={{ width: "100%" }}
                required
                id="outlined-required"
                label="List Title"
                variant="outlined"
                onChange={(e) => {
                  setInputValue(e.target.value);
                }}
                size='small'
              />
              <ButtonGroup aria-label="secondary button group">
                <Button
                  type="submit"
                  size="small"
                  variant="contained"
                  sx={{ marginRight: "10px" }}
                >
                  Add list
                </Button>
                <Button
                  variant="text"
                  size="small"
                  onClick={() => {
                    setShowAddListField(false);
                  }}
                >
                  X
                </Button>
              </ButtonGroup>
            </Stack>
          </form>
        )}
      </Card>
    </div>
  );
};

export default CreateList;
